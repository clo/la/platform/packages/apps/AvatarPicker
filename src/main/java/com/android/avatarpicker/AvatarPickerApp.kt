/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.avatarpicker

import android.app.Application
import android.content.Intent
import com.android.avatarpicker.data.ColoredIconsRepository
import com.android.avatarpicker.data.IllustrationsRepository
import com.android.avatarpicker.data.MediaRepository
import com.android.avatarpicker.domain.GroupedSelectableItemsUseCase
import com.android.avatarpicker.domain.GroupedSelectableItemsUseCaseImpl
import com.android.avatarpicker.ui.ResultHandler
import com.android.avatarpicker.ui.ResultHandlerImpl
import com.android.avatarpicker.ui.details.items.ItemViewComposerImpl

class AvatarPickerApp : Application(), AvatarProviderApp {
    private var _groupedSelectableItems: GroupedSelectableItemsUseCase? = null
    private var _resultHandler: ResultHandler? = null
    private val _itemComposer = ItemViewComposerImpl()

    override fun getGroupedSelectableItems(intent: Intent) =
        _groupedSelectableItems ?: GroupedSelectableItemsUseCaseImpl(
            MediaRepository(), ColoredIconsRepository(resources), IllustrationsRepository(resources)
        ).also { _groupedSelectableItems = it }

    override fun getResultHandler() =
        _resultHandler ?: ResultHandlerImpl(applicationContext).also { _resultHandler = it }

    override fun getItemViewComposer() = _itemComposer
}
