/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.avatarpicker.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.material3.windowsizeclass.calculateWindowSizeClass
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.android.avatarpicker.AvatarProviderApp
import com.android.avatarpicker.domain.CAMERA
import com.android.avatarpicker.domain.PHOTO_PICKER
import com.android.avatarpicker.ui.bottombar.BottomActionBar
import com.android.avatarpicker.ui.details.DetailsList
import com.android.avatarpicker.ui.details.items.UiState
import com.android.avatarpicker.ui.info.InfoCard
import com.android.avatarpicker.ui.theme.AvatarPickerTheme

class AvatarPickerActivity : ComponentActivity() {

    @OptIn(ExperimentalMaterial3WindowSizeClassApi::class, ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)

        val activityViewModel = (application as AvatarProviderApp).let {
            ActivityViewModel(it.getGroupedSelectableItems(intent), it.getResultHandler())
        }
        val itemViewComposer = (application as AvatarProviderApp).getItemViewComposer()

        setContent {
            AvatarPickerTheme {
                val windowSize = calculateWindowSizeClass(this)
                Surface(
                    contentColor = MaterialTheme.colorScheme.surfaceContainer,
                ) {
                    val showOnePane = (windowSize.widthSizeClass != WindowWidthSizeClass.Expanded)
                    val currentResult by activityViewModel.resultHandler.uiState.collectAsState()
                    activityViewModel.resultHandler.getSelected()?.typeId?.let { typeId: Int ->
                        if (typeId == CAMERA || (typeId == PHOTO_PICKER)) {
                            setResultAndFinish(activityViewModel.resultHandler)
                        }
                    }
                    Box(
                        Modifier.fillMaxSize(), contentAlignment = Alignment.Center
                    ) {
                        AdaptivePane(showOnePane = showOnePane,
                            startPane = { InfoCard(activityViewModel.infoViewModel) },
                            endPane = {
                                it.DetailsList(activityViewModel.detailsViewModel, itemViewComposer)
                            },
                            bottom = {
                                BottomActionBar(currentResult is UiState.Success<*>, {
                                    setResultAndFinish(activityViewModel.resultHandler)
                                }, {
                                    activityViewModel.resultHandler.unselect()
                                    this@AvatarPickerActivity.finish()
                                })
                            })

                        if (currentResult is UiState.Loading) {
                            Box(
                                Modifier.fillMaxSize()
                                    .background(MaterialTheme.colorScheme.onSurface.copy(.5f)),
                                contentAlignment = Alignment.Center
                            ) {
                                CircularProgressIndicator(
                                    modifier = Modifier.width(64.dp),
                                    color = MaterialTheme.colorScheme.secondary,
                                    trackColor = MaterialTheme.colorScheme.surfaceVariant,
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    fun setResultAndFinish(resultHandler: ResultHandler){
        val result = resultHandler.toResultIntent(this)
        resultHandler.unselect()
        this.setResult(RESULT_OK, result)
        this.finish()
    }
}
