/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.avatarpicker.ui.details.items.media

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.android.avatarpicker.ui.details.items.GetCircleModifier
import com.android.avatarpicker.ui.details.items.ResourceViewModel

@Composable
fun MediaIcon(model: ResourceViewModel, select: () -> Unit) {
    Image(painter = painterResource(id = model.drawableId),
        contentDescription = stringResource(id = model.descriptionId),
        colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.primary),
        modifier = GetCircleModifier().clickable { select() })
}